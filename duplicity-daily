#!/bin/bash
#
# Backup directories daily. Configuration at /etc/duplicity-daily

# Requires dupltool
[ -x /usr/bin/dupltool ] || exit 0

CONFIG=/etc/duplicity-daily.conf
grep -q ^ENABLE=yes $CONFIG || exit 0

# Detail backup options
OPTIONS=( $(grep ^OPTIONS= $CONFIG | sed 's/^OPTIONS=//') )

# The default backup target
TARGET="$(grep ^TARGET= -m1 $CONFIG | sed 's/^TARGET=//')"

# Backup lines start with full path, optionally with =target
# No spaces in pathname or target, e.g.
# /home
# /opt/other/x=user@host/backup/x

SRCS=( $(grep ^/ $CONFIG) )

# add --exlude option for any path of $SRCS that extends $1
excludes() {
    local SRC X
    for SRC in "${SRCS[@]}" ; do
	SRC=${SRC%%=*}
	[ "$SRC" = "$1" ] && continue
	[ -z "${SRC%$1*}" ] && echo -n " --exclude $SRC"
    done
}

for SRC in "${SRCS[@]}" ; do
    if [ "${SRC#*=}" = "$SRC" ] ; then
	DST=$TARGET$SRC
    else
	DST="${SRC##*=}"
	SRC="${SRC%%=*}"
    fi
    echo "** Backing up $SRC to $DST" 
    echo dupltool $(excludes "$SRC") ${OPTIONS[*]} $SRC pexpect+scp://$DST
    dupltool $(excludes "$SRC") ${OPTIONS[*]} $SRC pexpect+scp://$DST
done |& logger -t backup
