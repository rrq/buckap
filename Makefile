BINDIR = $(DESTDIR)/usr/bin
ETCDIR = $(DESTDIR)/etc
CRONDAILY = $(DESTDIR)/etc/cron.daily

compile:
	: # do nothing

$(BINDIR)/% $(ETCDIR)/% $(CRONDAILY)/%: %
	mkdir -p $$(dirname $@)
	cp $< $@

INSTALLTARGETS = $(BINDIR)/dupltool $(BINDIR)/restore.sh
INSTALLTARGETS += $(ETCDIR)/duplicity-daily.conf
INSTALLTARGETS += $(CRONDAILY)/duplicity-daily

install: $(INSTALLTARGETS)

deb:
	PREFIX= INCLUDE_PREFIX=/usr dpkg-buildpackage -us -uc --build=full
